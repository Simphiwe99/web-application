package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/bootstrap-min.css\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/main.css\">\n");
      out.write("        <title>Login</title>\n");
      out.write("          <style>\n");
      out.write("              body{\n");
      out.write("                  background-image: url(\"img/Company Logo.jpg\");\n");
      out.write("       \n");
      out.write("              }\n");
      out.write("              .img{\n");
      out.write("                  opacity: 0.2;\n");
      out.write("              }\n");
      out.write("              .fnt{\n");
      out.write("                  \n");
      out.write("                  font-style: oblique;\n");
      out.write("                  font-size: 45px;\n");
      out.write("              }\n");
      out.write("              .center{\n");
      out.write("                  margin: auto;\n");
      out.write("                  width: 450px;\n");
      out.write("                  border: 3px solid black;\n");
      out.write("                  padding: 20px;\n");
      out.write("                  background-color:  silver;\n");
      out.write("                  padding-top: 80px;\n");
      out.write("                  \n");
      out.write("              }\n");
      out.write("              .container{\n");
      out.write("                  padding-top: 45px;\n");
      out.write("              }\n");
      out.write("             \n");
      out.write("              \n");
      out.write("              span.psw{\n");
      out.write("                  float: right;\n");
      out.write("                  \n");
      out.write("              }\n");
      out.write("              \n");
      out.write("              @media screen and (max-width: 300px){\n");
      out.write("                  span.psw{\n");
      out.write("                      display: block;\n");
      out.write("                      float: none;\n");
      out.write("                  }\n");
      out.write("              }\n");
      out.write("    </style>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div class=\"center container\">\n");
      out.write("           \n");
      out.write("            <div class=\"panel panel-primary\">\n");
      out.write("               \n");
      out.write("            <br><br><br><br>\n");
      out.write("            <center><h1 class=\"fnt\">Login</h1></center><br><br>\n");
      out.write("            <form action=\"controller\" method=\"POST\">\n");
      out.write("                <div class=\"form-group\">              \n");
      out.write("                    <label><b>UserName:</b></label>\n");
      out.write("                    <input type=\"text\" name=\"txtusername\" placeholder=\"UserName\" required>\n");
      out.write("                </div>\n");
      out.write("                <br><br>\n");
      out.write("                <div class=\"form-group\">  \n");
      out.write("                    <label><b>Password:</b></label>\n");
      out.write("                    <input type=\"password\" name=\"txtpassword\" placeholder=\"Password\" required>\n");
      out.write("                </div>\n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("                    <input type=\"submit\" value=\"Log In\" class=\" btn btn-secondary btn-block\">\n");
      out.write("                    <labeL>\n");
      out.write("                        <input type=\"checkbox\" checked=\"checked\" name=\"remember\">Remember Me\n");
      out.write("                    </labeL>\n");
      out.write("                    <span class=\"psw\">Create  :<a href=\"index.jsp\"><b>account</b></a></span>\n");
      out.write("                    <br><br> <span class=\"psw\">Forgot  :<a href=\"#\"><b>password</b></a></span>\n");
      out.write("                    \n");
      out.write("                    \n");
      out.write("               \n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("            \n");
      out.write("        </div>\n");
      out.write("       \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
