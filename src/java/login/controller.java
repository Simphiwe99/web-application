
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 *
 * @author konas
 */
public class controller extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
          
            
            String username=request.getParameter("txtusername");
            String password=request.getParameter("txtpassword");
            try{
                if(username!=null){
                    Class.forName("com.mysql.jdbc.Driver").newInstance();
                    Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/login","root","");
                    String Query = "select * from users where username=? and password=?";
                    PreparedStatement ps = conn.prepareStatement(Query);
                    ps.setString(1, username);
                    ps.setString(2, password);
                    ResultSet rs=ps.executeQuery();
                    if(rs.next()){
                        response.sendRedirect("loginValidation.jsp");
                    }else{
                       out.println("Login Failed!Try Again");
                    }
                }
            }catch(Exception ex){
                out.println("Exception :"+ex.getMessage());
            }
        }
    }

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
