
package users;


public class Employee1 extends Admin {
    
    private String name;
    private String lastname;
    private String email;
    private String password;
    private int ID;
     
    public Employee1(String password, String email){
        super(password,email);
        
        this.ID =ID;
        this.name=name;
        this.lastname=lastname;
        this.email=email;
        this.password=password;
    }
    public int getID(){
        return ID;
    }
    public String getName(){
        return name;
    }
    public String getLastname(){
        return lastname;
    }
    @Override
    public String getEmail(){
        return email;
    }
    @Override
    public String getPassword(){
        return password;
    }
    @Override
    public String toString(){
        return String.format("%s %s %n", getID(),getName(),getLastname(),getEmail(),getPassword());
    }
}
