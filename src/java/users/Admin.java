
package users;


public abstract class Admin {
    private final String email;
    private final String password;
    
    public Admin(String email, String password){
        this.email = email;
        this.password = password;
    }
    public String getEmail(){
        return email;
    }
    public String getPassword(){
        return password;
    }
    @Override
    public abstract String toString();
}
