/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package registration.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import net.javaguides.registration.model.controller.Registration;

/**
 *
 * @author konas
 */
public class registerDao {

    private String firstName;
    private String lastName;
    private String gender;
    private String email;
    private String password;
    private String number;
    public int registerUser(Registration user)throws ClassNotFoundException, SQLException{
        String INSERT_USER_SQL = "INSERT INTO user" +
                "(firstName, lastName, gender, email, password, number) VALUES"+
                "(?,?,?,?,?,?);";
        
        int result = 0;
        
        Class.forName("com.sql.jdbc.Driver");
        
        try {
           Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/test","root","");
           
           //Creating a statement using connection objects
            PreparedStatement ps = conn.prepareStatement(INSERT_USER_SQL);
            
        ps.setString(1, firstName);
        ps.setString(2, lastName);
        ps.setString(3, gender);
        ps.setString(4, email);
        ps.setString(5, password);
        ps.setString(6, number);
        
            System.out.println(ps);
            
            //Step 3: Execute the query nor updating the query
            result = ps.executeUpdate();
    }catch(SQLException  e){
        //this processes the sql exeception
        e.printStackTrace();
    }
        return result;
        
    }
}
    

