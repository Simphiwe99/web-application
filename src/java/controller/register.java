/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.javaguides.registration.model.controller.Registration;
import registration.dao.registerDao;

@WebServlet("/register")
public class register extends HttpServlet{
    private static final long serialVersionUID = 1L;
    
    private registerDao reg = new registerDao();
    
    public register(){
        super();
    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        response.getWriter().append("Served at: ").append(request.getContextPath());
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Web Pages/index.jsp");
        dispatcher.forward(request, response);
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String gender = request.getParameter("gender");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String number = request.getParameter("number");
        
        Registration regi = new Registration();
        regi.setFirstName(firstName);
        regi.setLastName(lastName);
        regi.setGender(gender);
        regi.setEmail(email);
        regi.setPassword(password);
        regi.setNumber(number);
      
       try{
            try {
                reg.registerUser(regi);
            } catch (SQLException ex) {
                Logger.getLogger(register.class.getName()).log(Level.SEVERE, null, ex);
            }
           
       }catch(ClassNotFoundException e){
           e.printStackTrace();
       }
        RequestDispatcher dispatcher = request.getRequestDispatcher("/Web Pages/registerDetails.jsp");
        dispatcher.forward(request, response);
    }
    }

